package com.boohoo.redis.service;

public interface SessionService {
	public Long getSessionCount();
}
